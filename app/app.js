﻿(function () {
    "use strict"
    angular.module("app", []);
    angular.module("app").controller("appcontroller", appController);

    function appController() {
        var that = this;
        this.staff = data;
        this.selectedDepartment = '';
        this.selectedStaffName = '';
        this.selectedStaffGender = '';
        this.filterStaff = function (value) {
            return this.filterStaffName(value) && this.filterStaffGender(value)
        }.bind(this);

        this.filterStaffName = function (value) {
            return !this.selectedStaffName
                || value.firstName.toLowerCase().indexOf(this.selectedStaffName.toLowerCase()) > -1
                || value.lastName.toLowerCase().indexOf(this.selectedStaffName.toLowerCase()) > -1;

        }.bind(this);

        this.filterStaffGender = function (value) {
            return !this.selectedStaffGender || value.gender == this.selectedStaffGender;
        }.bind(this);

        var ctx = document.getElementById('piechart');

        this.createChartData = function () {
            var dic = {};

            this.staff.forEach(function (x) {
                if (that.filterStaff(x)) {
                    if (dic[x.department]) {
                        dic[x.department]++;
                    } else {
                        dic[x.department] = 1
                    }
                }
            });

            var rtn = [['Dept.', 'No. Staff']];
            for (var val in dic) {
                rtn.push([val, dic[val]])
            }
            return rtn;
        }.bind(this);

        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable(
               that.createChartData()
            );

            var options = {
                title: 'Staff by department'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);

            that.updateChart = function () {
                data = google.visualization.arrayToDataTable(
                   that.createChartData()
                );
                chart.draw(data, options);
            }
        }
    }

    var data = [
        { id: 1, firstName: "Andy", lastName: "Anders", gender: "M", department: "Sales" },
        { id: 2, firstName: "Bert", lastName: "Bertington", gender: "M", department: "Marketing" },
        { id: 3, firstName: "Clair", lastName: "Claris", gender: "F", department: "Sales" },
        { id: 4, firstName: "Dhara", lastName: "Davies", gender: "F", department: "Accounts" },
        { id: 5, firstName: "Edward", lastName: "Edmonton", gender: "M", department: "Accounts" },
        { id: 6, firstName: "Fiona", lastName: "Fenwick", gender: "F", department: "Marketing" },
        { id: 7, firstName: "Garry", lastName: "Gibson", gender: "M", department: "Sales" },
        { id: 8, firstName: "Harriet", lastName: "Harrison", gender: "F", department: "Accounts" },
        { id: 9, firstName: "Edwarda", lastName: "Edmonton", gender: "F", department: "Sales" },
    ]
}());